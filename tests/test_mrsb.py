import pytest
from unittest.mock import Mock

import mrsb

@pytest.fixture
def client():
    mrsb.FLASK_APP.config['TESTING'] = True
    with mrsb.FLASK_APP.test_client() as client:
        yield client

def test_hello(client):
    rv = client.get('/hi')
    assert b'Hello' in rv.data


def test_quick_response(client):
    rv = client.get('/')
    assert '204' in rv.status


def test_handle_message(client):
    mrsb.JIRA_CLIENT = Mock()
    returner = Mock()
    returner.fields.summary = "BEST ISSUE EVER"
    mrsb.JIRA_CLIENT.issue = Mock(return_value=returner)
    mrsb.SLACK_CLIENT = Mock()
    rv = mrsb.handle_message("some message text CORE-100", "Channel000", "10001")
    assert 204 in rv
    mrsb.JIRA_CLIENT.issue.assert_called_with("CORE-100")
    mrsb.SLACK_CLIENT.api_call.assert_called()


