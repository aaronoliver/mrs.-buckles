"""Flask app to act as Jira-linking Slack bot.

This app listens for requests sent from Slack when messages are posted in
"listened to" channels. It then replies with links to Jira issues that were
found.

"""

import os
import re
import json
import sys
import logging

import github
import jira
import sentry_sdk

from threading import Thread
from splitio import get_factory
from splitio.exceptions import TimeoutException
from flask import Flask, request
from slackclient import SlackClient


logging.basicConfig(level=logging.DEBUG)
FLASK_APP = Flask(__name__)

FLASK_APP.config.from_object('mrsb.default_settings')
try:
    FLASK_APP.config.from_envvar('MRSB_SETTINGS')
except:
    pass #keep going...for now
sentry_sdk.init(FLASK_APP.config["SENTRY_URL"], environment=FLASK_APP.config.get("SENTRY_ENV"))

JIRA_HOME = FLASK_APP.config["JIRA_SERVER"]
SLACK_CLIENT = SlackClient(FLASK_APP.config['BOT_TOKEN'])

JIRA_CLIENT = None

if JIRA_HOME:
    JIRA_CLIENT = jira.JIRA({'server': JIRA_HOME},
                                   basic_auth=(FLASK_APP.config["JIRA_USERNAME"], 
                                   FLASK_APP.config["JIRA_PASSWORD"]))

g = github.Github(FLASK_APP.config['GITHUB_USER'], FLASK_APP.config['GITHUB_TOKEN'])

config = {'ready' : 5000}

if FLASK_APP.config.get('SPLIT_API_KEY'):
    try:
        factory = get_factory(FLASK_APP.config['SPLIT_API_KEY'], config=config)
        split = factory.client()
    except TimeoutException:
        # The SDK failed to initialize in time. Abort!
        sys.exit()

@FLASK_APP.route("/hi", methods=["POST", "GET"])
def hello():
    return "Hello, Mrs. Buckles!"
    
@FLASK_APP.route("/", methods=["POST", "GET"])
def quick_response():

    if not request.data:
        return no_content_response()

    slack_event_json = json.loads(request.data)
    print(slack_event_json)
    message_text, message_channel, thread_ts = _unpack_slack_json(slack_event_json)
    if not message_text:
        return no_content_response()
    
    thr = Thread(target=handle_message, args=[message_text, message_channel, thread_ts])
    thr.start()
    return no_content_response()


def handle_message(message_text, message_channel, thread_ts):
    """Respond to incoming messages from Slack."""
    with FLASK_APP.app_context():
        pull_requests = get_pull_requests_from_string(message_text)
        issue_keys = get_issue_keys_from_string(message_text)
        text = None
        attachments = None

        if pull_requests:
            blocks = [_get_pr_text_block(_get_pr_summary(pull_requests))]
        elif len(issue_keys) == 1:
            block =  _get_single_issue_block(issue_keys[0])
            if not block:
                return no_content_response()
            blocks = [block]
        elif len(issue_keys) > 1:
            blocks = get_multiple_jira_messages_block(issue_keys)
            blocks.append(_multi_issue_button_block(issue_keys))
        else:
            return no_content_response()

        # post the message and link to Jira or Github
        payload = {
                "thread_ts": thread_ts,
                "channel": message_channel,
                "blocks": blocks,
                }
        SLACK_CLIENT.api_call("chat.postMessage",
                    **payload
                )
        return no_content_response()

def get_jira_message_block(jira_issue_key):
    return _get_single_issue_block(jira_issue_key)

def get_multiple_jira_messages_block(jira_issue_keys):
    return [_get_single_issue_block(issue_key) for issue_key in jira_issue_keys]


def _unpack_slack_json(slack_event_json):
    """Pull useful parts out of Slack's event json payload.
    Returns a tuple of message text, channel id, and thread timestamp.

    """
    if "event" not in slack_event_json:
        return (None, None, None)
    message_text = message_text_from_slack_event(slack_event_json)
    message_channel = _get_channel_from_event(slack_event_json)
    user =  slack_event_json['token']

    attributes = dict()
    attributes["channel"] = message_channel
    treatment = split.get_treatment(user, 'Threaded_Replies', attributes)
    if treatment == 'on': 
        thread_key = "ts"
    elif treatment == 'off':
        thread_key = "thread_ts"
    else:
        thread_key = "thread_ts"
    thread_ts = slack_event_json['event'].get(thread_key)

    return (message_text, message_channel, thread_ts)


def _get_single_issue_block(issue_key):
    try:
        issue = JIRA_CLIENT.issue(issue_key)
    except:
        return None
    text = (
        f"[<{JIRA_HOME}/browse/{issue_key}|{issue_key}>] - " +issue.fields.summary
            )
    return _get_jira_issue_block(text)


def _get_jira_issue_block(text):
    return {"type": "context", "elements": [ {"type": "mrkdwn", "text": text}]}


def _get_pr_text_block(pull_requests):
    text = pull_requests
    return {"type": "section", "text": {"type": "mrkdwn", "text": text}}


def _get_pr_summary(pull_requests):
    print (pull_requests)
    text = ""
    for pr_tuple in pull_requests:
        repo = g.get_repo(pr_tuple[0])
        pr = repo.get_pull(pr_tuple[1])
        reviews = pr.get_reviews()
        text += "*[pull " + str(pr_tuple[1]) + "]* _" + pr.title + '_ *' + pr.state.upper() + ' - '+ str(reviews.totalCount)+ ' reviews*\n'         
    return text

def _multi_issue_button_block(issue_keys):
    """returns an attachment list for a button linking to JQL for several
    issues.
    """
    url = JIRA_HOME + "/issues/?jql=key%20in%20(" \
                    + ','.join(issue_keys) +")"
    fallback_message = "View " \
                       + str(len(issue_keys)) + " issues"
    button_text = " View "  + str(len(issue_keys)) + " issues"
    return {
               "type": "actions", 
               "elements": [
                   {
                       "type": "button", 
                       "text": {
                           "type": "plain_text",
                           "text": button_text
                    },
                    "style": "primary",
                    "url": url
                }
                 ]}
                

def get_urls(string):
    """
    >>> get_urls("hi this is http://aaronoliver.com/one/two")
    ['http://aaronoliver.com/one/two']

    >>> get_urls("Need eyes on >>> >>> >>> https://github.com/Veritix/Fan.Account.Services/pull/222")
    ['https://github.com/Veritix/Fan.Account.Services/pull/222']
    
    >>> get_urls("Need eyes on  <https://github.com/Veritix/Fan.Account.Services/pull/222>")
    ['https://github.com/Veritix/Fan.Account.Services/pull/222']
    """
    urls = []
    for s in string.split(' '):
       if 'http' in s:
        urls.append(s.strip('<>')) #slack puts angle brackets around urls
    return urls
 

def get_issue_keys_from_string(string):
    """
    >>> get_issue_keys_from_string("Nothing to see here. No issue numbers")
    []

    >>> get_issue_keys_from_string("A phone number is not an issue number 123-456-7890")
    []

    >>> get_issue_keys_from_string("DEMO-100")
    ['DEMO-100']

    >>> get_issue_keys_from_string("AC2-100")
    ['AC2-100']

    >>> get_issue_keys_from_string("My Favorite issue is DEMO-200")
    ['DEMO-200']

    >>> get_issue_keys_from_string("Even jammedDEMO-300Issues work ok")
    ['DEMO-300']

    >>> get_issue_keys_from_string("one is DEMO-100, DEMO-200 AND DEMO-330")
    ['DEMO-100', 'DEMO-200', 'DEMO-330']

    >>> get_issue_keys_from_string("one is DEMO-100, DEMO-200 AND DEMO-330 and DEMO-100 again")
    ['DEMO-100', 'DEMO-200', 'DEMO-330']

    """
    jira_issue_key_regex = '[A-Z][A-Z0-9]+-\d+'
    match_list = re.findall(jira_issue_key_regex, string)
    return list(set(match_list))

def get_pull_requests_from_string(string):
    """
    >>> get_pull_requests_from_string("Need eyes on https://github.com/Veritix/Fan.Account.Services/pull/222")
    [('Veritix/Fan.Account.Services', 222)]

    >>> get_pull_requests_from_string("What's up with >>> https://github.com/Veritix/Fan.Account.Services/pull/333")
    [('Veritix/Fan.Account.Services', 333)]

    >>> get_pull_requests_from_string("What's up with https://github.com/Veritix/Fan.Account.Services/pull/1333>. hi")
    [('Veritix/Fan.Account.Services', 1333)]

    >>> get_pull_requests_from_string("What's up with nothing")
    []

    >>> get_pull_requests_from_string("What's up with https://aaronoliver.com")
    []

    >>> get_pull_requests_from_string("Need eyes on https://github.com/Veritix/Fan.Account.Services/pull/222 and https://github.com/Veritix/Fan.Account.Services/pull/333")
    [('Veritix/Fan.Account.Services', 222), ('Veritix/Fan.Account.Services', 333)]
    """
    ids = []
    urls = get_urls(string)
    for url in urls:
        if 'github' in url:
            repo = url.split('/')[-4:-2]
            pr_id = url.split('/')[-1:]
            pr_id = re.sub('[^0-9]', '', pr_id[0])
            pr_tuple = ("/".join(repo), int(pr_id))
            ids.append(pr_tuple)
    return ids

def message_text_from_slack_event(event_json):
    """Get the "message" that was posted to a slack channel.

    Takes an event json dict/object and finds non-bot text. Basically
    extracts the message typed by a person.

    """
    if "event" not in event_json:
        return None

    if event_json["event"].get("subtype") == "bot_message":
        return None
    if "text" not in event_json["event"]:
        return None
    message_text = event_json["event"]["text"]
    return message_text


def _get_channel_from_event(event_json):
    """Get the channel id from the json sent by Slack."""
    message_channel = event_json["event"]["channel"]
    return message_channel

def no_content_response():
    """Returns a 204-tuple suitable for a Flask app response."""
    return ('', 204)


def get_issue_summaries(issue_keys):
    """Query Jira to get summaries for a list of issue keys.

    returns a list of summaries. May not have a summary for every key if the key isn't found
    """
    if not issue_keys:
        return None
    issues = JIRA_CLIENT.search_issues("key in (" + ','.join(issue_keys) + ")")
    if not issues:
        return None
    text = ""
    for issue in issues:
        text += f"[<{JIRA_HOME}/browse/{issue.key}|{issue.key}>] - {issue.fields.summary}\n"
    return text

if  __name__ == "__main__":
    FLASK_APP.run()
